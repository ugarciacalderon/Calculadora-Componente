#!/usr/bin/env python
"""
fecha Enero-30-2018
__autor__: ugarcia
Programacion por Componentes
"""
import sys
import commands

if __name__=="__main__":
    divisor = int(sys.argv[1])
    dividendo = int(sys.argv[2])
    div = 0
    contador = 0
    #aux = dividendo
    if divisor > dividendo:
        div = divisor
        divisor = dividendo
        dividendo = div
        aux = div
    else:
        aux = dividendo

    while True:
        aux = int(commands.getoutput("/usr/bin/env python resta.py "+str(aux)+" "+str(divisor)))
        #aux = aux-divisor
        contador +=1
        if aux<=0:
            break
    if aux <0:
        print"resultado: ",contador-1
        print"resto: ",aux+divisor
    else:
        print"el resultado es: ",contador
        print"resto es 0"
