#!/usr/bin/env python
"""
fecha Enero-30-2018
__autor__: ugarcia
Programacion por Componentes
"""
import sys
import commands

if __name__ == "__main__":
    operacion = sys.argv[1]    #parametro para operacion(string)
    param2 = sys.argv[2]   #parametro numerico(int)
    param3 = sys.argv[3]   #parametro numerico(int)

    param1 = operacion.lower()
    result = commands.getoutput("/usr/bin/env python "+operacion+".py "+str(param2)+" "+str(param3))
    print result
