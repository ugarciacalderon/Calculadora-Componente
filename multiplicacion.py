#!/usr/bin/env python
"""
fecha Enero-30-2018
__autor__: ugarcia
Programacion por Componentes
"""
import sys
import commands

if __name__=="__main__":
    param1 = int(sys.argv[1])
    param2 = int(sys.argv[2])
    result = param2
    for i in range(0,param1-1):
        result = int(commands.getoutput("/usr/bin/env python suma.py "+str(result)+" "+str(param2)))
    print result
